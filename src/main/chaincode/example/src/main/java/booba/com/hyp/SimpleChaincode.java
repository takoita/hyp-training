package booba.com.hyp;

import com.google.protobuf.ByteString;
import io.netty.handler.ssl.OpenSsl;
import org.hyperledger.fabric.protos.peer.Chaincode;
import org.hyperledger.fabric.shim.ChaincodeBase;
import org.hyperledger.fabric.shim.ChaincodeStub;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class SimpleChaincode extends ChaincodeBase {

    private static Logger logger = LoggerFactory.getLogger(SimpleChaincode.class);

    @Override
    public Response init(ChaincodeStub stub) {
        try {
            logger.info("Init Java Simple chaincode");
            String func = stub.getFunction();
            if (!func.equals("init")) {
                return newErrorResponse("function other than init is not supported");
            }
            List<String> args = stub.getParameters();
            if (args.size() != 4) {
                newErrorResponse("Incorrect number of arguments. Expecting 4");
            }
            String account1Key = args.get(0);
            int account1Value = Integer.parseInt(args.get(1));
            String account2Key = args.get(2);
            int account2Value = Integer.parseInt(args.get(3));

            logger.info(String.format("account %s, value = %s; acount %s, value %s", account1Key, account1Value,
                    account2Key, account2Value));

            stub.putStringState(account1Key, args.get(1));
            stub.putStringState(account2Key, args.get(3));

            return newSuccessResponse();
        } catch (NumberFormatException e) {
            return newErrorResponse();
        }
    }

    @Override
    public Response invoke(ChaincodeStub stub) {
        try {
            logger.info("Invoke java simple chaincode");
            String func = stub.getFunction();
            List<String> params = stub.getParameters();
            if (func.equals("invoke")) {
                return invoke(stub, params);
            }
            if (func.equals("delete")) {
                return delete(stub, params);
            }
            if (func.equals("query")) {
                return query(stub, params);
            }
            return newErrorResponse("Invalid invoke funtion name. Expecting one of: [\"invoke\", \"delete\", " +
                    "\"quzery\"]");
        } catch (Throwable e) {
            return newErrorResponse();
        }

    }

    private Response query(ChaincodeStub stub, List<String> params) {
        if (params.size() != 1) {
            return newErrorResponse("Incorrect number of arguments. Expecting name of the person to query.");
        }
        String key = params.get(0);
        String val = stub.getStringState(key);
        if (val == null) {
            return newErrorResponse(String.format("Error: state for %s is null", key));
        }
        logger.info(String.format("Query Reponse:\nName: %s, Amount: %s\n", key, val));
        return newSuccessResponse(val, ByteString.copyFrom(val, StandardCharsets.UTF_8).toByteArray());
    }

    private Response delete(ChaincodeStub stub, List<String> params) {
        if (params.size() != 1) {
            return newErrorResponse("Incorrect number of arguments. Expecting 1");
        }
        String key = params.get(0);
        stub.delState(key);
        return newSuccessResponse();

    }

    private Response invoke(ChaincodeStub stub, List<String> params) {
        if (params.size() != 3) {
            return newErrorResponse("Incorrect number of arguments. Expecting 3");
        }
        String accountFromKey = params.get(0);
        String accountToKey = params.get(1);

        String accountFromValueStr = stub.getStringState(accountFromKey);
        if (accountFromValueStr == null) {
            return newErrorResponse(String.format("Entity %s not founf", accountFromKey));
        }
        int accountFromValue = Integer.parseInt(accountFromValueStr);

        String accountToValueStr = stub.getStringState(accountToKey);
        if (accountToValueStr == null) {
            return newErrorResponse(String.format("Entity %s not found", accountToKey));
        }
        int accountToValue = Integer.parseInt(accountToValueStr);

        int amount = Integer.parseInt(params.get(2));

        if (amount > accountFromValue) {
            return newErrorResponse(String.format("not enough money in account %s", accountFromKey));
        }

        accountFromValue -= amount;
        accountToValue += amount;

        logger.info(String.format("new value of A: %s", accountFromValue));
        logger.info(String.format("new value of B: %s", accountToValue));

        stub.putStringState(accountFromKey, Integer.toString(accountFromValue));
        stub.putStringState(accountToKey, Integer.toString(accountToValue));

        logger.info("Transfer complete");

        return newSuccessResponse("invoke finished successfully",
                ByteString.copyFrom(accountFromKey + ": " + accountFromValue + " " + accountToKey + ": " +
                        ": " + accountToValue, StandardCharsets.UTF_8).toByteArray());
    }


    public static void main(String[] args) {
        System.out.println("OpenSSL avaliable: " + OpenSsl.isAvailable());
        new SimpleChaincode().start(args);
    }

}
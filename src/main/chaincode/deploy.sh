#!//bin/bash

set -o xtrace

export CHANNEL_NAME=mychannel

peer channel create -o orderer.aboubakarkoita.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/aboubakarkoita.com/orderers/orderer.aboubakarkoita.com/msp/tlscacerts/tlsca.aboubakarkoita.com-cert.pem



peer channel join -b mychannel.block



export CORE_PEER_ADDRESS=peer1.org1.aboubakarkoita.com:7051 export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/peers/peer1.org1.aboubakarkoita.com/tls/ca.crt

peer channel join -b mychannel.block

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/users/Admin@org2.aboubakarkoita.com/msp export CORE_PEER_ADDRESS=peer0.org2.aboubakarkoita.com:7051 export CORE_PEER_LOCALMSPID="Org2MSP" export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/peers/peer0.org2.aboubakarkoita.com/tls/ca.crt

peer channel join -b mychannel.block

export CORE_PEER_ADDRESS=peer1.org2.aboubakarkoita.com:7051 export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/peers/peer1.org2.aboubakarkoita.com/tls/ca.crt

peer channel join -b mychannel.block






export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/users/Admin@org1.aboubakarkoita.com/msp export CORE_PEER_ADDRESS=peer0.org1.aboubakarkoita.com:7051

export CORE_PEER_LOCALMSPID="Org1MSP"

export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/peers/peer0.org1.aboubakarkoita.com/tls/ca.crt

peer channel update -o orderer.aboubakarkoita.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/Org1MSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/aboubakarkoita.com/orderers/orderer.aboubakarkoita.com/msp/tlscacerts/tlsca.aboubakarkoita.com-cert.pem

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/users/Admin@org2.aboubakarkoita.com/msp

export CORE_PEER_ADDRESS=peer0.org2.aboubakarkoita.com:7051 export CORE_PEER_LOCALMSPID="Org2MSP"

export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/peers/peer0.org2.aboubakarkoita.com/tls/ca.crt

peer channel update -o orderer.aboubakarkoita.com:7050 -c $CHANNEL_NAME -f ./channel-artifacts/Org2MSPanchors.tx --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/aboubakarkoita.com/orderers/orderer.aboubakarkoita.com/msp/tlscacerts/tlsca.aboubakarkoita.com-cert.pem






export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/users/Admin@org1.aboubakarkoita.com/msp
export CORE_PEER_ADDRESS=peer0.org1.aboubakarkoita.com:7051
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/peers/peer0.org1.aboubakarkoita.com/tls/ca.crt

peer chaincode install -n firstcc -v 1.0 -l java -p /opt/gopath/src/github.com/chaincode/example

sleep 30

export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/users/Admin@org2.aboubakarkoita.com/msp

export CORE_PEER_ADDRESS=peer0.org2.aboubakarkoita.com:7051

export CORE_PEER_LOCALMSPID="Org2MSP"

export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org2.aboubakarkoita.com/peers/peer0.org2.aboubakarkoita.com/tls/ca.crt

peer chaincode install -n firstcc -v 1.0 -l java -p /opt/gopath/src/github.com/chaincode/example


sleep 30




export CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/users/Admin@org1.aboubakarkoita.com/msp
export CORE_PEER_ADDRESS=peer0.org1.aboubakarkoita.com:7051
export CORE_PEER_LOCALMSPID="Org1MSP"
export CORE_PEER_TLS_ROOTCERT_FILE=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.aboubakarkoita.com/peers/peer0.org1.aboubakarkoita.com/tls/ca.crt

peer chaincode instantiate -o orderer.aboubakarkoita.com:7050 --tls --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/aboubakarkoita.com/orderers/orderer.aboubakarkoita.com/msp/tlscacerts/tlsca.aboubakarkoita.com-cert.pem -C $CHANNEL_NAME -n firstcc -l java -v 1.0 -c '{"Args":["init","a", "100", "b","200"]}' -P "AND ('Org1MSP.peer','Org2MSP.peer')"



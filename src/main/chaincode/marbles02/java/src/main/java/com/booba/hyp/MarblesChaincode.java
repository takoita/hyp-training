//package com.booba.hyp;
//
//import org.apache.commons.lang3.StringUtils;
//import org.hyperledger.fabric.shim.ChaincodeBase;
//import org.hyperledger.fabric.shim.ChaincodeStub;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import sun.util.locale.provider.LocaleServiceProviderPool;
//
//import java.util.List;
//
///**
// * ====CHAINCODE EXECUTION SAMPLES (CLI) ==================
// * <p>
// * <p>
// * ==== Invoke marbles ====
// * <p>
// * peer chaincode invoke -C myc1 -n marbles -c '{"Args":["initMarble","marble1","blue","35","tom"]}'
// * <p>
// * peer chaincode invoke -C myc1 -n marbles -c '{"Args":["initMarble","marble2","red","50","tom"]}'
// * <p>
// * peer chaincode invoke -C myc1 -n marbles -c '{"Args":["initMarble","marble3","blue","70","tom"]}'
// * <p>
// * peer chaincode invoke -C myc1 -n marbles -c '{"Args":["transferMarble","marble2","jerry"]}'
// * <p>
// * peer chaincode invoke -C myc1 -n marbles -c '{"Args":["transferMarblesBasedOnColor","blue","jerry"]}'
// * <p>
// * peer chaincode invoke -C myc1 -n marbles -c '{"Args":["delete","marble1"]}'
// * <p>
// * <p>
// * ==== Query marbles ====
// * <p>
// * peer chaincode query -C myc1 -n marbles -c '{"Args":["readMarble","marble1"]}'
// * <p>
// * peer chaincode query -C myc1 -n marbles -c '{"Args":["getMarblesByRange","marble1","marble3"]}'
// * <p>
// * peer chaincode query -C myc1 -n marbles -c '{"Args":["getHistoryForMarble","marble1"]}'
// * <p>
// * <p>
// * Rich Query (Only supported if CouchDB is used as state database):
// * <p>
// * peer chaincode query -C myc1 -n marbles -c '{"Args":["queryMarblesByOwner","tom"]}'
// * <p>
// * peer chaincode query -C myc1 -n marbles -c '{"Args":["queryMarbles","{\"selector\":{\"owner\":\"tom\"}}"]}'
// * <p>
// * <p>
// * Rich Query with Pagination (Only supported if CouchDB is used as state database):
// * <p>
// * peer chaincode query -C myc1 -n marbles -c '{"Args":["queryMarblesWithPagination","{\"selector\":{\"owner\":\"tom
// * \"}}","3",""]}'
// * <p>
// * <p>
// * INDEXES TO SUPPORT COUCHDB RICH QUERIES
// * <p>
// * <p>
// * <p>
// * Indexes in CouchDB are required in order to make JSON queries efficient and are required for
// * <p>
// * any JSON query with a sort. As of Hyperledger Fabric 1.1, indexes may be packaged alongside
// * <p>
// * chaincode in a META-INF/statedb/couchdb/indexes directory. Each index must be defined in its own
// * <p>
// * text file with extension *.json with the index definition formatted in JSON following the
// * <p>
// * CouchDB index JSON syntax as documented at:
// * <p>
// * http://docs.couchdb.org/en/2.1.1/api/database/find.html#db-index
// * <p>
// * <p>
// * <p>
// * This marbles02 example chaincode demonstrates a packaged
// * <p>
// * index which you can find in META-INF/statedb/couchdb/indexes/indexOwner.json.
// * <p>
// * For deployment of chaincode to production environments, it is recommended
// * <p>
// * to define any indexes alongside chaincode so that the chaincode and supporting indexes
// * <p>
// * are deployed automatically as a unit, once the chaincode has been installed on a peer and
// * <p>
// * instantiated on a channel. See Hyperledger Fabric documentation for more details.
// * <p>
// * <p>
// * <p>
// * If you have access to the your peer's CouchDB state database in a development environment,
// * <p>
// * you may want to iteratively test various indexes in support of your chaincode queries.  You
// * <p>
// * can use the CouchDB Fauxton interface or a command line curl utility to create and update
// * <p>
// * indexes. Then once you finalize an index, include the index definition alongside your
// * <p>
// * chaincode in the META-INF/statedb/couchdb/indexes directory, for packaging and deployment
// * <p>
// * to managed environments.
// * <p>
// * <p>
// * <p>
// * In the examples below you can find index definitions that support marbles02
// * <p>
// * chaincode queries, along with the syntax that you can use in development environments
// * <p>
// * to create the indexes in the CouchDB Fauxton interface or a curl command line utility.
// * <p>
// * <p>
// * <p>
// * <p>
// * Example hostname:port configurations to access CouchDB.
// * <p>
// * <p>
// * <p>
// * To access CouchDB docker container from within another docker container or from vagrant environments:
// * <p>
// * http://couchdb:5984/
// * <p>
// * <p>
// * <p>
// * Inside couchdb docker container
// * <p>
// * http://127.0.0.1:5984/
// * <p>
// * <p>
// * Index for docType, owner.
// * <p>
// * <p>
// * <p>
// * Example curl command line to define index in the CouchDB channel_chaincode database
// * <p>
// * curl -i -X POST -H "Content-Type: application/json" -d "{\"index\":{\"fields\":[\"docType\",\"owner\"]},\"name
// * \":\"indexOwner\",\"ddoc\":\"indexOwnerDoc\",\"type\":\"json\"}" http://hostname:port/myc1_marbles/_index
// * <p>
// * <p>
// * <p>
// * <p>
// * Index for docType, owner, size (descending order).
// * <p>
// * <p>
// * <p>
// * Example curl command line to define index in the CouchDB channel_chaincode database
// * <p>
// * curl -i -X POST -H "Content-Type: application/json" -d "{\"index\":{\"fields\":[{\"size\":\"desc\"},{\"docType
// * \":\"desc\"},{\"owner\":\"desc\"}]},\"ddoc\":\"indexSizeSortDoc\", \"name\":\"indexSizeSortDesc\",
// * \"type\":\"json\"}" http://hostname:port/myc1_marbles/_index
// * <p>
// * <p>
// * Rich Query with index design doc and index name specified (Only supported if CouchDB is used as state database):
// * <p>
// * peer chaincode query -C myc1 -n marbles -c '{"Args":["queryMarbles","{\"selector\":{\"docType\":\"marble\",
// * \"owner\":\"tom\"}, \"use_index\":[\"_design/indexOwnerDoc\", \"indexOwner\"]}"]}'
// * <p>
// * <p>
// * Rich Query with index design doc specified only (Only supported if CouchDB is used as state database):
// * <p>
// * peer chaincode query -C myc1 -n marbles -c '{"Args":["queryMarbles","{\"selector\":{\"docType\":{\"$eq\":\"marble
// * \"},\"owner\":{\"$eq\":\"tom\"},\"size\":{\"$gt\":0}},\"fields\":[\"docType\",\"owner\",\"size\"],\"sort\":[{
// * \"size\":\"desc\"}],\"use_index\":\"_design/indexSizeSortDoc\"}"]}'
// */
//public class MarblesChaincode extends ChaincodeBase {
//    private static Logger logger = LoggerFactory.getLogger(MarblesChaincode.class);
//
//    @Override
//    public Response init(ChaincodeStub stub) {
//        logger.info("Initiating Java MarblesChaincode chaincode");
//        return newSuccessResponse();
//    }
//
//    @Override
//    public Response invoke(ChaincodeStub stub) {
//        try {
//            String func = stub.getFunction();
//            List<String> params = stub.getParameters();
//            logger.info("invoking MarblesChaincode chaincode function: " + func);
//            switch (func) {
//                case "initMarble":
//                    return initMarble(stub, params);
//                case "transferMarble":
//                    return transferMarble(stub, params);
//                case "transferMarblesBasedOnColor":
//                    return transferMarblesBasedOnColor(stub, params);
//                case "delete":
//                    return delete(stub, params);
//                case "readMarble":
//                    return readMarble(stub, params);
//                case "queryMarblesByOwner":
//                    return queryMarblesByOwner(stub, params);
//                case "queryMarbles":
//                    return queryMarbles(stub, params);
//                case "getHistoryForMarble":
//                    return getHistoryForMarble(stub, params);
//                case "getMarblesByRange":
//                    return getMarblesByRange(stub, params);
//                case "getMarblesByRangeWithPagination":
//                    return getMarblesByRangeWithPagination(stub, params);
//                case "queryMarblesWithPagination":
//                    return queryMarblesWithPagination(stub, params);
//
//            }
//            logger.error("invalid invoke function name: {}", func);
//            return newErrorResponse("invalid invoke function name: " + func);
//        } catch (Throwable e) {
//            return newErrorResponse();
//        }
//    }
//
//
//    /**
//     * Create and store a new marble in the chaincode state.
//     *
//     * @param stub
//     * @param params
//     * @return status
//     */
//    private Response initMarble(ChaincodeStub stub, List<String> params) {
//        logger.info("initMarble");
//        if (params.size() != 4) {
//            logger.error("incorrect number of arguments. Expecting 4, but found: {}", params.size());
//            return newErrorResponse("incorrect number of arguments. Expecting 4, but found:" + params.size());
//        }
//        String marbleName = params.get(0);
//        String color = params.get(1);
//        String owner = params.get(2);
//        String sizeString = params.get(3);
//
//        if(StringUtils.isEmpty(marbleName)){
//            logger.error("1st argument(marble name) must be non-empty string");
//            return newErrorResponse("1st argument(marble name) must be non-empty string");
//        }
//        if(StringUtils.isEmpty(color)){
//            logger.error("2nd argument(color) must be non-empty string");
//            return newErrorResponse("2nd argument(color) must be non-empty string");
//        }
//        if(StringUtils.isEmpty(owner)){
//            logger.error("3rd argument(owner) must be non-empty string");
//            return newErrorResponse("3rd argument(owner) must be non-empty string");
//        }
//        if(StringUtils.isEmpty(sizeString)){
//            logger.error("4th argument() must be non-empty string");
//            return newErrorResponse("3rd argument(owner) must be non-empty string");
//        }
//        int size = Integer.parseInt(sizeString);
//
//
//
//        Marble marble =
//
//
//    }
//
//
//    private Response queryMarblesWithPagination(ChaincodeStub stub, List<String> params) {
//        return null;
//    }
//
//    private Response getMarblesByRangeWithPagination(ChaincodeStub stub, List<String> params) {
//        return null;
//    }
//
//    private Response getMarblesByRange(ChaincodeStub stub, List<String> params) {
//        return null;
//    }
//
//    private Response getHistoryForMarble(ChaincodeStub stub, List<String> params) {
//
//
//    }
//
//    private Response queryMarbles(ChaincodeStub stub, List<String> params) {
//        return null;
//    }
//
//    private Response queryMarblesByOwner(ChaincodeStub stub, List<String> params) {
//        return null;
//    }
//
//    private Response readMarble(ChaincodeStub stub, List<String> params) {
//        return null;
//    }
//
//    private Response delete(ChaincodeStub stub, List<String> params) {
//        return null;
//    }
//
//    private Response transferMarblesBasedOnColor(ChaincodeStub stub, List<String> params) {
//        return null;
//    }
//
//    private Response transferMarble(ChaincodeStub stub, List<String> params) {
//
//    }
//
//}

#!/usr/bin/env bash

docker logs -f peer0.org1.example.com 2>&1 | grep \\[peer\\]

peer node start --logging-level=debug
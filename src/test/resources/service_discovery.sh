#!/usr/bin/env bash

PEER_TLS_CA=./crypto/peerOrganizations/org1.example.com/tlsca/tlsca.org1.example.com-cert.pem

USER_KEY=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/User1\@org1.example.com/msp/keystore/9045cf6649abaa511e60b3f4c15baaedc9ed14aa58c74f4204be7eaf13c5b529_sk
USER_CERT=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/users/User1\@org1.example.com/msp/signcerts/User1\@org1.example.com-cert.pem


#USER_KEY=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp/keystore/9ba08012a3754a3239a8b3cc3632bd13a1b4e5d4fe71edc624acefe92109a47e_sk
#USER_CERT=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/org1.example.com/peers/peer0.org1.example.com/msp/signcerts/peer0.org1.example.com-cert.pem


discover --configFile conf.yaml --peerTLSCA  $CORE_PEER_TLS_ROOTCERT_FILE --userKey $USER_KEY  --userCert $USER_CERT --MSP Org1MSP saveConfig

# Peer membership query:
discover --configFile conf.yaml peers --channel mychannel  --server peer0.org1.example.com:7051
discover --configFile conf.yaml peers --channel mychannel  --server peer0.org1.example.com:7051  | jq .[0].Identity | sed "s/\\\n/\n/g" | sed "s/\"//g"  | openssl x509 -text -noout

# Configuration query:
discover --configFile conf.yaml config --channel mychannel  --server peer0.org1.example.com:7051

# Endorsers query:
discover --configFile conf.yaml endorsers --channel mychannel  --server peer0.org1.example.com:7051 --chaincode mycc

#!/usr/bin/env bash

export FABRIC_CA_HOME="/home/vagrant/app/hyperledger/fabric-home/ca/server"


fabric-ca-server init -b admin:adminpw

fabric-ca-server start -b admin:adminpw


export FABRIC_CA_CLIENT_HOME="/home/vagrant/app/hyperledger/fabric-home/ca/clients/admin"

fabric-ca-client enroll -u http://admin:adminpw@localhost:7054

fabric-ca-client register --id.name admin2 --id.affiliation org1.department1 --id.attrs 'hf.Revoker=true,admin=true:ecert'
# Password: YRcVYUJtgbLk

fabric-ca-client register --id.name peer1 --id.type peer --id.affiliation org1.department1 --id.secret peer1pw

export FABRIC_CA_CLIENT_HOME=/home/vagrant/app/hyperledger/fabric-home/ca/clients/peer1

fabric-ca-client enroll -u http://peer1:peer1pw@localhost:7054

fabric-ca-client reenroll

export FABRIC_CA_CLIENT_HOME="/home/vagrant/app/hyperledger/fabric-home/ca/clients/admin"

fabric-ca-client revoke -e peer1


fabric-ca-client gencrl

fabric-ca-client gencrl --caname "" --revokedafter 2017-09-13T16:39:57-08:00 --revokedbefore 2017-09-21T16:39:57-08:00 -M ~/msp


fabric-ca-client register --id.name user1 --id.secret user1pw --id.type user --id.affiliation org1 --id.attrs 'app1Admin=true:ecert,email=user1@gmail.com'

fabric-ca-client enroll -u http://user1:user1pw@localhost:7054 --enrollment.attrs "email,phone:opt"



fabric-ca-client register --id.name peer2 --id.type peer --id.affiliation org1.department1 --id.secret peer2pw

fabric-ca-client register --id.name peer3 --id.type peer --id.affiliation org1.department1 --id.secret peer3pw

fabric-ca-client register --id.name peer4 --id.type peer --id.affiliation org1.department1 --id.secret peer4pw


fabric-ca-client identity list --id peer2

fabric-ca-client identity list

fabric-ca-client identity add user1 --json '{"secret": "user1pw", "type": "user", "affiliation": "org1", "max_enrollments": 1, "attrs": [{"name": "hf.Revoker", "value": "true"}]}'


fabric-ca-client identity modify user1 --json '{"secret": "newPassword", "affiliation": ".", "attrs": [{"name": "hf.Regisrar.Roles", "value": "peer,client"},{"name": "hf.Revoker", "value": "true"}]}'


fabric-ca-client affiliation add org1.dept1

fabric-ca-client affiliation modify org2 --name org3

fabric-ca-client affiliation remove org2

fabric-ca-client affiliation list --affiliation org2

fabric-ca-client affiliation list


fabric-ca-client certificate list


export FABRIC_CA_SERVER_BCCSP_DEFAULT=PKCS11
export FABRIC_CA_SERVER_BCCSP_PKCS11_LIBRARY=/usr/local/lib/softhsm/libsofthsm2.so
export FABRIC_CA_SERVER_BCCSP_PKCS11_PIN=98765432 FABRIC_CA_SERVER_BCCSP_PKCS11_LABEL=ForFabric
